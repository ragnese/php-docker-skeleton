FROM php:7-cli-alpine AS base
WORKDIR my-app
RUN apk --no-cache add git unzip
COPY --from=composer:1.8 /usr/bin/composer /opt/bin/

FROM base AS dev
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

FROM base AS prod
COPY . .
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
        COMPOSER_ALLOW_SUPERUSER=1 php /opt/bin/composer install --no-dev -n && \
        apk del git unzip && \
        rm /opt/bin/composer
