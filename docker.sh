#!/bin/bash

set -euo pipefail

function print_usage {
    cat <<EOF
Usage: $0 [command]

build                               Build a development docker image and cleanup old versions
clean                               Stop and remove all project-related docker images
composer <command> args...          Run composer inside of the development image
shell                               Open a shell on the development image
test                                Run unit tests on the development image
deploy                              Build the production image
-h, --help                          Display this message
EOF
}

if [[ "$#" -eq 0 ]]; then
    print_usage
    exit
fi

case "$1" in
    "build")
        docker-compose build
        docker-compose run --rm app /opt/bin/composer install --no-interaction
        ;;
    "clean")
        docker-compose down -v --rmi all --remove-orphans > /dev/null 2>&1
        docker system prune -f --volumes
        ;;
    "composer")
        docker-compose run --rm app /opt/bin/composer "${@:2}"
        ;;
    "shell")
        docker-compose run --rm --entrypoint sh app
        ;;
    "test")
        docker-compose run --rm app ./vendor/bin/phpunit tests
        ;;
    "deploy")
        docker-compose -f docker-compose.yml down -v --rmi all --remove-orphans > /dev/null 2>&1
        docker-compose -f docker-compose.yml build --force-rm --no-cache
        docker system prune -f --volumes
        ;;
    "-h"|"--help"|*)
        print_usage
esac
