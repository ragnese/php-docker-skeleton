<?php

use \PHPUnit\Framework\TestCase;

final class AppTest extends TestCase
{
    public function testMethodOne(): void
    {
        self::assertEquals(1, (new App())->methodOne());
    }
}
